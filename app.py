from flask import Flask, jsonify, render_template
import os

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')


@app.route('/_get_files')
def get_files():
    d = os.path.join(os.getcwd(), 'static/data')
    files = []
    for f in os.listdir(d):
        if (os.path.splitext(f))[1] == '.csv':
            files.append(f)
    return jsonify(files)


if __name__ == '__main__':
    app.run(debug=True)
