# HJA Hobo Visualization
This is an open source project, and as such, it is free for you to use for your project! We hope it's useful to you. 
It also has no support, no 
maintenance, and no regular updates. The original developer built it for a specific project and has now moved on to 
another position and other projects. Any updates are from the user community as needed to troubleshoot and fix bugs, or 
to develop enhancements. Please give back by sharing yours!

This is a simple d3 based tool for visualizing HJA HOBO flagged files.  It shows a time-series of temperature and light-intensity data and all places that have been identified as possible flagged records in the series.  Initially, the data is subset so that noon-time temperatures are shown as a line series, but as the user zooms in to a window of less than 10 days, the line series turns into the set of actual sensor temperatures/intensities.  Flagged values are shown at all times regardless of zoom level.

This application is designed to be a stand-alone flask application that will be run as localhost on the user's workstation.  Following are instructions for simple setup:

### Clone the git repository to your system
1. Open a command shell and navigate to a parent directory where you want to put the application
2. Type: `git clone https://<username>@bitbucket.org/hjandrews/hja-hobo-visualize.git hja-hobo-visualize` using your username without the angle brackets.

This creates a directory called `hja-hobo-visualize` that has all the files you need to run the application.

### Populate the 'static/data' directory with flagged files
Copy all flagged files produced from hja-hobo-clean into the 'static/data' directory.  The application looks for CSV files in this directory to populate the dropdown list in the application.

### Make sure that flask is installed
Ideally, the user has set up a Python virtual environment for isolating this from their main Python install.  For instructions, see documentation for the [virtualenv](https://virtualenv.pypa.io/en/stable/) package (and the [virtualenvwrapper-win](https://github.com/davidmarble/virtualenvwrapper-win/) package if on Windows).

Once the virtual environment is set up, run the following command:
* `pip install -U flask`

### Start the flask application
* Get into the `hja-hobo-visualize` folder
* Open a command shell
* Type `python app.py`
* Open a browser and navigate to `http://localhost:5000/`

### Stop the flask application
Once finished with the visualization, close the browser tab and hit 'Ctrl-C' (Windows).
*Need documentation on how to stop on OSX.* 
