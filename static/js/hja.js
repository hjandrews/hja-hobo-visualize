// Modeled after https://bl.ocks.org/mbostock/34f08d5e11952a80609169b7917d4172

// SVG container
var svg = d3.select("svg");

// Margins for temperature, intensity and context frames
var margin1 = {top: 20, right: 20, bottom: 530, left: 40};
var margin2 = {top: 290, right: 20, bottom: 260, left: 40};
var margin3 = {top: 560, right: 20, bottom: 200, left: 40};

// Width of all frames
var width = +svg.attr("width") - margin1.left - margin1.right;

// Heights of temperature, intensity and context frames
var height1 = 250;
var height2 = 250;
var height3 = 40;

// Container for temperature frame
var focus_temp = svg.append("g")
  .attr("class", "focus")
  .attr("transform", "translate(" + margin1.left + "," + margin1.top + ")");

// Container for intensity frame
var focus_intensity = svg.append("g")
  .attr("class", "focus")
  .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

// Container for context frame
var context = svg.append("g")
  .attr("class", "context")
  .attr("transform", "translate(" + margin3.left + "," + margin3.top + ")");

// Size of dots
var dotRadius = 3;

// Time parser
var parseTime = d3.timeParse("%Y-%m-%d %H:%M:%S");

// Scales for frames
var x1 = d3.scaleTime().range([0, width]);
var x2 = d3.scaleTime().range([0, width]);
var x3 = d3.scaleTime().range([0, width]);
var y1 = d3.scaleLinear().range([height1, 0]);
var y2 = d3.scaleLinear().range([height2, 0]);
var y3 = d3.scaleLinear().range([height3, 0]);
 
var xAxis1 = d3.axisBottom(x1)
  .ticks(10)
  .tickSize(-height1)
  .tickPadding(8);

var xAxis2 = d3.axisBottom(x2)
  .ticks(10)
  .tickSize(-height2)
  .tickPadding(8);

var xAxis3 = d3.axisBottom(x3);

var yAxis1 = d3.axisLeft(y1)
  .ticks(10)
  .tickSize(-width)
  .tickPadding(8);

var yAxis2 = d3.axisLeft(y2)
  .ticks(10)
  .tickSize(-width)
  .tickPadding(8);

var brush = d3.brushX()
  .extent([[0,0], [width, height3]])
  .on("brush end", brushed);

var zoom1 = d3.zoom()
  .scaleExtent([1, 5000])
  .translateExtent([[0, 0], [width, height1]])
  .extent([[0, 0], [width, height1]])
  .on("zoom", zoomed);

var zoom2 = d3.zoom()
  .scaleExtent([1, 5000])
  .translateExtent([[0, 0], [width, height2]])
  .extent([[0, 0], [width, height2]])
  .on("zoom", zoomed);

var line1 = d3.line()
  .curve(d3.curveMonotoneX)
  .x(function(d) { return x1(d.DATE_TIME); })
  .y(function(d) { return y1(d.TEMP_C); })

var line2 = d3.line()
  .curve(d3.curveMonotoneX)
  .x(function(d) { return x2(d.DATE_TIME); })
  .y(function(d) { return y2(d.INTENSITY); })

var line3 = d3.line()
  .curve(d3.curveMonotoneX)
  .x(function(d) { return x3(d.DATE_TIME); })
  .y(function(d) { return y3(d.TEMP_C); })

svg.append("defs").append("clipPath")
    .attr("id", "mask")
  .append("rect")
    .attr("width", width)
    .attr("height", height1);

var tempOutlierScale = d3.scaleOrdinal()
  .domain(["JUMP", "TIME_GAP", "EXTREME", "SNOW", "SPIKE"])
  .range(["orange", "blue", "red", "green", "purple"]);

svg.append("g")
  .attr("class", "tempLegend")
  .attr("transform", "translate(1070,35)");

var tempLegend = d3.legendColor()
  .shape("path", d3.symbol().type(d3.symbolCircle).size(80)())
  .shapePadding(5)
  .scale(tempOutlierScale);

svg.select(".tempLegend")
  .call(tempLegend);

var intensityOutlierScale = d3.scaleOrdinal()
  .domain(["EXTREME", "HIGH_INTENSITY", "RADIATION_BIAS", 
      "LOW_DAYLIGHT", "NIGHTTIME"])
  .range(["orange", "blue", "red", "green", "purple"]);

svg.append("g")
  .attr("class", "intensityLegend")
  .attr("transform", "translate(1070,305)");

var intensityLegend = d3.legendColor()
  .shape("path", d3.symbol().type(d3.symbolCircle).size(80)())
  .shapePadding(5)
  .scale(intensityOutlierScale);

svg.select(".intensityLegend")
  .call(intensityLegend);

// Variable to hold on to all unfiltered data
var allData;

// Initialize the pulldown menu by retrieving all data files and populating
// the menu
d3.json('/_get_files', function(error, json_data) {
  var select = d3.select('#file-choice');

  // Create menu items for each file found
  select.selectAll('option')
    .data(json_data)
    .enter()
      .append('option')
      .attr('value', function(d) { return d; })
      .text(function (d) { return d; });

  // Set up the handler for the change event
  select
    .on('change', function(d) {
      draw(d3.select(this).property('value'));
    });

  // Finally, select the top file and render it
  setUp();
  draw(select.property('value'));
});

// Read in raw data and send to callback 
var readData = function(fn, cb) {
  d3.csv(fn, function(d) {
    d.DATE_TIME = parseTime(d.DATE_TIME);
    d.TEMP_C = +d.TEMP_C;
    d.INTENSITY = +d.INTENSITY;
    d.DAYLIGHT = +d.DAYLIGHT;
    d.TIME_GAP = +d.TIME_GAP;
    d.EXTREME_TEMP = +d.EXTREME_TEMP;
    d.JUMP = +d.JUMP;
    d.EXTREME_INTENSITY = +d.EXTREME_INTENSITY;
    d.HIGH_INTENSITY = +d.HIGH_INTENSITY;
    d.RADIATON_BIAS_INTENSITY = +d.RADIATION_BIAS_INTENSITY;
    d.LOW_DAYLIGHT_INTENSITY = +d.LOW_DAYLIGHT_INTENSITY;
    d.NIGHTTIME_INTENSITY = +d.NIGHTTIME_INTENSITY;
    d.SNOW = +d.SNOW;
    d.TEMP_SPIKE = +d.TEMP_SPIKE;
    return d;
  }, function(error, data) {
    allData = data;
    if (error) throw error;
  
    var sdata = data.filter(function(d) {
      return (
          d.DATE_TIME.getHours() === 12 && 
          d.DATE_TIME.getMinutes() >= 0 &&
          d.DATE_TIME.getMinutes() < 20
      );
    });
  
    var tempOutliers = data.filter(function(d) {
      return (
        d.TIME_GAP === 1 ||
        d.EXTREME_TEMP === 1 || 
        d.JUMP === 1 || 
        d.SNOW === 1 || 
        d.TEMP_SPIKE === 1
      );
    });

    var intensityOutliers = data.filter(function(d) {
      return (
        d.EXTREME_INTENSITY === 1 ||
        d.HIGH_INTENSITY === 1 ||
        d.RADIATION_BIAS_INTENSITY === 1 ||
        d.LOW_DAYLIGHT_INTENSITY === 1 ||
        d.NIGHTTIME_INTENSITY === 1
      );
    });

    cb({
      sdata: sdata,
      tempOutliers: tempOutliers,
      intensityOutliers: intensityOutliers
    });
  });
};

var setUp = function() {
  focus_temp.append("path")
    .attr("class", "line lineon");

  focus_intensity.append("path")
    .attr("class", "line lineon");

  focus_temp.append("g")
    .attr("class", "axis axis--x")
    .attr("transform", "translate(0," + height1 + ")");

  focus_intensity.append("g")
    .attr("class", "axis axis--x")
    .attr("transform", "translate(0," + height2 + ")");

  focus_temp.append("g")
    .attr("class", "axis axis--y");
  
  focus_intensity.append("g")
    .attr("class", "axis axis--y");

  focus_temp.append("g")
    .attr("class", "backgroundDots");

  focus_intensity.append("g")
    .attr("class", "backgroundDots");

  focus_temp.append("g")
    .attr("class", "dots");

  focus_intensity.append("g")
    .attr("class", "dots");

  context.append("path")
    .attr("class", "line lineon");

  context.append("g")
    .attr("class", "axis axis--x")
    .attr("transform", "translate(0," + height3 + ")");

  context.append("g")
    .attr("class", "brush");

  svg.append("rect")
    .attr("class", "zoom1")
    .attr("width", width)
    .attr("height", height1)
    .attr("transform", "translate(" + margin1.left + "," + margin1.top + ")");

  svg.append("rect")
    .attr("class", "zoom2")
    .attr("width", width)
    .attr("height", height2)
    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");
};

function drawTempDots(selection) {
  selection
    .attr("cx", function(d) { return x1(d.DATE_TIME); })
    .attr("cy", function(d) { return y1(d.TEMP_C); })
    .attr("fill", function(d) {
      if ((d.JUMP === 1) && d.TEMP_SPIKE === 0) {
        return "orange";
      } else if (d.TIME_GAP === 1) {
        return "blue";
      } else if (d.EXTREME_TEMP === 1) {
        return "red";
      } else if (d.SNOW === 1) {
        return "green";
      } else if (d.EXTREME_TEMP === 0 && d.TEMP_SPIKE === 1) {
        return "purple";
      }
    });
}

function drawIntensityDots(selection) {
  selection
    .attr("cx", function(d) { return x2(d.DATE_TIME); })
    .attr("cy", function(d) { return y2(d.INTENSITY); })
    .attr("fill", function(d) {
      if (d.EXTREME_INTENSITY === 1) {
        return "orange";
      } else if (d.HIGH_INTENSITY === 1) {
        return "blue";
      } else if (d.RADIATION_BIAS === 1) {
        return "red";
      } else if (d.LOW_DAYLIGHT_INTENSITY === 1) {
        return "green";
      } else if (d.NIGHTTIME_INTENSITY === 1) {
        return "purple";
      }
    });
}


var draw = function(fn) {
  fn = './static/data/' + fn;
  readData(fn, function(obj) {
    var sdata = obj.sdata;
    var tempOutliers = obj.tempOutliers;
    var intensityOutliers = obj.intensityOutliers;
  
    var yBufferTemp = buffer(
      d3.extent(sdata, function(d) { return d.TEMP_C; }), 1.1);
  
    x1.domain(d3.extent(sdata, function(d) { return d.DATE_TIME; }));
    y1.domain(yBufferTemp);

    var yBufferIntensity = buffer(
      d3.extent(sdata, function(d) { return d.INTENSITY; }), 1.1);
  
    x2.domain(d3.extent(sdata, function(d) { return d.DATE_TIME; }));
    y2.domain(yBufferIntensity);

    x3.domain(x1.domain());
    y3.domain(y1.domain());

    focus_temp.select(".line")
      .datum(sdata)
      .attr("d", line1);

    focus_intensity.select(".line")
      .datum(sdata)
      .attr("d", line2);
  
    var dots = focus_temp.select(".dots").selectAll("circle")
      .data(tempOutliers);

    dots
      .call(drawTempDots);

    dots.enter().append("circle")
      .attr("r", dotRadius)
      .call(drawTempDots);

    dots.exit().remove();

    var dots = focus_intensity.select(".dots").selectAll("circle")
      .data(intensityOutliers);

    dots
      .call(drawIntensityDots);

    dots.enter().append("circle")
      .attr("r", dotRadius)
      .call(drawIntensityDots);

    dots.exit().remove();

    focus_temp.select(".axis--x")
      .call(xAxis1);
  
    focus_intensity.select(".axis--x")
      .call(xAxis2);

    focus_temp.select(".axis--y")
      .call(yAxis1);
  
    focus_intensity.select(".axis--y")
      .call(yAxis2);

    context.select(".line")
      .datum(sdata)
      .attr("d", line3);
 
    context.select(".axis--x")
      .call(xAxis3);
  
    context.select(".brush")
      .call(brush)
      .call(brush.move, x1.range());
 
    svg.select(".zoom1")
      .call(zoom1);

    svg.select(".zoom2")
      .call(zoom2);
  });
};

function brushed() {
  // If a zoom event triggered this function, it has been triggered from 
  // one of the focus windows - just return in this case
  if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom")
    return;

  // Get the current range specified, either the selected region or the
  // full range by default
  var s = d3.event.selection || x3.range();

  // Set the domain of the focus windows to reflect the currently selected
  // x (time) range
  var mapped = s.map(x3.invert, x3);
  x1.domain(mapped);
  x2.domain(mapped);

  // Redraw the lines of the focus windows based on the new domains
  focus_temp.select(".line").attr("d", line1);
  focus_intensity.select(".line").attr("d", line2);

  // Redraw the x axes of the focus windows based on the new domains
  focus_temp.select(".axis--x").call(xAxis1);
  focus_intensity.select(".axis--x").call(xAxis2);

  // Rescale the transform for all zoom elements 
  var t = d3.zoomIdentity
    .scale(width / (s[1] - s[0]))
    .translate(-s[0], 0);
  svg.select(".zoom1").call(zoom1.transform, t);
  svg.select(".zoom2").call(zoom2.transform, t);
  var newXScale = t.rescaleX(x3);

  // Redraw the x-ccordinates of the outlier dots in the focus windows
  focus_temp.select(".dots").selectAll("circle")
    .attr("cx", function(d) { return newXScale(d.DATE_TIME); });
  focus_intensity.select(".dots").selectAll("circle")
    .attr("cx", function(d) { return newXScale(d.DATE_TIME); });

  // Determine whether or not to show lines or dots based on x domain
  showDataPoints();
}

function zoomed() {
  // If a brush event triggered this function, it has been triggered from 
  // the context window - just return in this case
  if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush")
    return;

  // Obtain the new transform and rescale elements based on it
  var t = d3.event.transform;
  var newXScale = t.rescaleX(x3);

  x1.domain(t.rescaleX(x3).domain());
  x2.domain(t.rescaleX(x3).domain());

  // Redraw the lines of the focus windows based on the new domains
  focus_temp.select(".line").attr("d", line1);
  focus_intensity.select(".line").attr("d", line2);

  // Redraw the x axes of the focus windows based on the new domains
  focus_temp.select(".axis--x").call(xAxis1);
  focus_intensity.select(".axis--x").call(xAxis2);

  // Redraw the x-ccordinates of the outlier dots in the focus windows
  focus_temp.select(".dots").selectAll("circle")
    .attr("cx", function(d) { return newXScale(d.DATE_TIME); });
  focus_intensity.select(".dots").selectAll("circle")
    .attr("cx", function(d) { return newXScale(d.DATE_TIME); });

  // Update the brush extent in the context window
  context.select(".brush").call(brush.move, x1.range().map(t.invertX, t));

  // Determine whether or not to show lines or dots based on x domain
  showDataPoints();
}

function buffer(extent, factor) {
  var halfRange = (extent[1] - extent[0]) / 2.0;
  return [
    extent[0] + halfRange * (1.0 - factor),
    extent[0] + halfRange * (1.0 + factor)
  ];
}

function showDataPoints() {
  var diff = x1.domain()[1] - x1.domain()[0];
  var dayDiff = ((((diff / 1000.0) / 60.0) / 60.0) / 24.0);
  var windowedData = allData.filter(function(d) {
    return (
      dayDiff < 10.0 &&
      d.DATE_TIME >= x1.domain()[0] &&
      d.DATE_TIME <= x1.domain()[1]
    );
  });

  if (windowedData.length) {
    // focus_temp.select('.line').attr("class", "line lineoff");
    // focus_intensity.select('.line').attr("class", "line lineoff");
  } else {
    // focus_temp.select('.line').attr("class", "line lineon");
    // focus_intensity.select('.line').attr("class", "line lineon");
  }

  var dots = focus_temp.select(".backgroundDots").selectAll("circle")
    .data(windowedData);

  dots
    .attr("cx", function(d) { return x1(d.DATE_TIME); })
    .attr("cy", function(d) { return y1(d.TEMP_C); });

  dots.enter().append("circle")
    .attr("r", dotRadius)
    .attr("cx", function(d) { return x1(d.DATE_TIME); })
    .attr("cy", function(d) { return y1(d.TEMP_C); });

  dots.exit().remove();

  var dots = focus_intensity.select(".backgroundDots").selectAll("circle")
    .data(windowedData);

  dots
    .attr("cx", function(d) { return x2(d.DATE_TIME); })
    .attr("cy", function(d) { return y2(d.INTENSITY); });

  dots.enter().append("circle")
    .attr("r", dotRadius)
    .attr("cx", function(d) { return x2(d.DATE_TIME); })
    .attr("cy", function(d) { return y2(d.INTENSITY); });

  dots.exit().remove();
}
